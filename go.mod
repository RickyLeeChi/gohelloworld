module m

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.15 // indirect
	github.com/stretchr/testify v1.4.0
)
