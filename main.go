package main

import (
	"m/controllers"

	"github.com/gin-gonic/gin"
)

func main() {

	setupServer().Run(":3000")
}

// The engine with all endpoints is now extracted from the main function
func setupServer() *gin.Engine {
	router := gin.Default()
	router.GET("/names/:name", controllers.GetName)
	router.POST("/names", controllers.PostName)

	return router
}
